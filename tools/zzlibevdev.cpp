/*
g++ -Wall -Wextra -g ./zzlibevdev.cpp -o zzlibevdev -levdev
*/

//#include <config.h>

//#include <assert.h>
#include <chrono>         // std::chrono::seconds
#include <cstdint>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/input.h>
//#include <libevdev.h>
//#include <./libevdev/libevdev.h>
#include <libevdev-1.0/libevdev/libevdev.h>
#include <sys/time.h>
#include <thread>         // std::this_thread::sleep_for
#include <unistd.h>


/* **************************************************************** */
static void fwriteBE( const char* src_pCac, uint8_t srcSize_Cu8, FILE* const file_Cpf )
{
	uint8_t		chEelm_u8 = srcSize_Cu8;
	do
	{
		chEelm_u8--;
		const int		char_i = src_pCac[ chEelm_u8 ];
		putc( char_i, file_Cpf );
	} while ( chEelm_u8 > 0 );
}

/* **************************************************************** */
/**	64bit Zeit berechnen aus
*/
uint64_t timeval2u64( const struct timeval* const timeval_CpCo )
{
	const long		usecMax_Cl = 1e6L + 0.5L;
	uint64_t		time_u64 = (uint64_t)timeval_CpCo->tv_sec * usecMax_Cl;
	if ( timeval_CpCo->tv_usec >= usecMax_Cl )
	{	/* Wert ist ausserhalb des erwarteten Bereichs. */
		abort();
	}
	time_u64 += timeval_CpCo->tv_usec;
	return time_u64;
}

uint8_t		keyBtnLeftCt_u8;
uint8_t		keyBtnRightCt_u8;
uint8_t		keyBtnMiddleCt_u8;
uint8_t		relXct_u8;
int8_t		relXvalue_i8;
uint8_t		relYct_u8;
int8_t		relYvalue_i8;
uint8_t		relZct_u8;
int8_t		relZvalue_i8;
uint8_t		synReportCt_u8;

enum rasterId_Eu8		: uint8_t
{
	RASTERID_synReport,	/**< */
	RASTERID_keyBtnLeft,	/**< */
	RASTERID_keyBtnRight,	/**< */
	RASTERID_keyBtnMiddle,	/**< */
	RASTERID_relX,	/**< */
	RASTERID_relY,	/**< */
	RASTERID_relZ,	/**< */
//	RASTERID_,	/**< */
}		rasterId_eu8;


/* **************************************************************** */
int main(void)
{
	struct libevdev *dev = NULL;
	int fd;
	int		rc_i = 1;
	fd = open("/dev/input/event6", O_RDONLY|O_NONBLOCK);
	rc_i = libevdev_new_from_fd(fd, &dev);
	if (rc_i < 0)
	{
		fprintf(stderr, "Failed to init libevdev (%s)\n", strerror(-rc_i));
		exit(1);
	}
	printf("Input device name: \"%s\"\n", libevdev_get_name(dev));
	printf("Input device ID: bus %#x vendor %#x product %#x\n",
		   libevdev_get_id_bustype(dev),
		   libevdev_get_id_vendor(dev),
		   libevdev_get_id_product(dev));
	if (!libevdev_has_event_type(dev, EV_REL) ||
		!libevdev_has_event_code(dev, EV_KEY, BTN_LEFT)) {
			printf("This device does not look like a mouse\n");
			exit(1);
	}

	FILE*		meas_pf = fopen( "/tmp/libevdevTp.bmf", "a+b" );
	do
	{
		struct input_event		ev;
		rc_i = libevdev_next_event( dev, LIBEVDEV_READ_FLAG_NORMAL, &ev );
		if ( rc_i == 0 )
		{
			if ( 1 )
			{	/* Ausgabe auf dem Bildschirm */
				printf("Event: %s %s %d\n",
					   libevdev_event_type_get_name(ev.type),
					   libevdev_event_code_get_name(ev.type, ev.code),
					   ev.value);
			}

			{	/* Messdatei fuellen. */
				switch ( ev.type )
				{
					case EV_SYN:
					{
						switch ( ev.code )
						{
							case SYN_REPORT:
							{
								rasterId_eu8 = RASTERID_synReport;
								synReportCt_u8++;
								break;
							}
							case SYN_CONFIG:
							{
								abort();
								break;
							}
							case SYN_MT_REPORT:
							{
								abort();
								break;
							}
							case SYN_DROPPED:
							{
								abort();
								break;
							}
							case SYN_MAX:
							{
								abort();
								break;
							}
						}
						break;
					}
					case EV_KEY:
					{
						switch ( ev.code )
						{
							case BTN_LEFT:
							{
								rasterId_eu8 = RASTERID_keyBtnLeft;
								keyBtnLeftCt_u8++;
								break;
							}
							case BTN_RIGHT:
							{
								rasterId_eu8 = RASTERID_keyBtnRight;
								keyBtnRightCt_u8++;
								break;
							}
							case BTN_MIDDLE:
							{
								rasterId_eu8 = RASTERID_keyBtnMiddle;
								keyBtnMiddleCt_u8++;
								break;
							}
							default:
							{
								abort();
								break;
							}
						}
						break;
					}
					case EV_REL:
					{
						switch ( ev.code )
						{
							case REL_X:
							{
								rasterId_eu8 = RASTERID_relX;
								relXct_u8++;
								relXvalue_i8 = ev.value;
								break;
							}
							case REL_Y:
							{
								rasterId_eu8 = RASTERID_relY;
								relYct_u8++;
								relYvalue_i8 = ev.value;
								break;
							}
							case REL_Z:
							{
								rasterId_eu8 = RASTERID_relZ;
								relZct_u8++;
								relZvalue_i8 = ev.value;
								break;
							}
							default:
							{
								abort();
								break;
							}
						}
						break;
					}
					case EV_ABS:
					{
						abort();
						break;
					}
					case EV_MSC:
					{
						abort();
						break;
					}
					case EV_SW:
					{
						abort();
						break;
					}
					case EV_LED:
					{
						abort();
						break;
					}
					case EV_SND:
					{
						abort();
						break;
					}
					case EV_REP:
					{
						abort();
						break;
					}
					case EV_FF:
					{
						abort();
						break;
					}
					case EV_PWR:
					{
						abort();
						break;
					}
					case EV_FF_STATUS:
					{
						abort();
						break;
					}
					case EV_MAX:
					{
						abort();
						break;
					}
				}
//				ev.code;
//				ev.value;

				const uint64_t		timeEv_Cu64 = timeval2u64( &ev.time );	/*< Zeitpunkt des Events. */

				uint64_t		time_u64;
				{	/* time_u64 bestimmen. */
					struct timeval		timeval_o;
					if (  gettimeofday( &timeval_o, NULL ) != 0 )
					{
						abort();
					}
					time_u64 = timeval2u64( &timeval_o );
				}

//				std::cout << time_u64 << " " << timeEv_Cu64 << " " <<  time_u64 - timeEv_Cu64 << std::endl;
				fwriteBE( reinterpret_cast< char* >( &rasterId_eu8 ), 		sizeof( rasterId_eu8 ), 	meas_pf );
				fwriteBE( reinterpret_cast< char* >( &time_u64 ), 			sizeof( time_u64 ), 		meas_pf );
				fwriteBE( reinterpret_cast< const char* >( &timeEv_Cu64 ),	sizeof( timeEv_Cu64 ), 		meas_pf );
				{	/* Delta Zeit zwischen aktuell und Event. */
					uint64_t		dt_u64 = time_u64 - timeEv_Cu64;
					uint32_t		max_Cu32 = (-1);
					if ( dt_u64 > max_Cu32 ) dt_u64 = max_Cu32;
					const uint32_t		dt_Cu32 = dt_u64;
					fwriteBE( reinterpret_cast< const char* >( &dt_Cu32 ), 			sizeof( dt_Cu32 ), 		meas_pf );
				}
				switch ( rasterId_eu8 )
				{
					case RASTERID_synReport:
					{
						fwriteBE( reinterpret_cast< char* >( &synReportCt_u8 ),	sizeof( synReportCt_u8 ), meas_pf );
						break;
					}
					case RASTERID_keyBtnLeft:
					{
						fwriteBE( reinterpret_cast< char* >( &keyBtnLeftCt_u8 ),	sizeof( keyBtnLeftCt_u8 ), meas_pf );
						break;
					}
					case RASTERID_keyBtnRight:
					{
						fwriteBE( reinterpret_cast< char* >( &keyBtnRightCt_u8 ),	sizeof( keyBtnRightCt_u8 ), meas_pf );
						break;
					}
					case RASTERID_keyBtnMiddle:
					{
						fwriteBE( reinterpret_cast< char* >( &keyBtnMiddleCt_u8 ),	sizeof( keyBtnMiddleCt_u8 ), meas_pf );
						break;
					}
					case RASTERID_relX:
					{
						fwriteBE( reinterpret_cast< char* >( &relXct_u8 ),	sizeof( relXct_u8 ), meas_pf );
						fwriteBE( reinterpret_cast< char* >( &relXvalue_i8 ),	sizeof( relXvalue_i8 ), meas_pf );
						break;
					}
					case RASTERID_relY:
					{
						fwriteBE( reinterpret_cast< char* >( &relYct_u8 ),	sizeof( relYct_u8 ), meas_pf );
						fwriteBE( reinterpret_cast< char* >( &relYvalue_i8 ),	sizeof( relYvalue_i8 ), meas_pf );
						break;
					}
					case RASTERID_relZ:
					{
						fwriteBE( reinterpret_cast< char* >( &relZct_u8 ),	sizeof( relZct_u8 ), meas_pf );
						fwriteBE( reinterpret_cast< char* >( &relZvalue_i8 ),	sizeof( relZvalue_i8 ), meas_pf );
						break;
					}
				}
			}
		}
		else
		{
//			printf("Event: else\n" );
			std::this_thread::sleep_for(  std::chrono::milliseconds( 10 )  );
		}
	} while (rc_i == 1 || rc_i == 0 || rc_i == -EAGAIN);

	fclose( meas_pf );
	return rc_i;
}
libinput
========

This fork
---------

The aim of this fork is to improve trackpoint use.

Through the files
- /tmp/setting_libinputTp.dpToFac_au16
	- Array (dpToFac_au16) with values used for converting to output pixel.
- /tmp/setting_libinputTp.dtUseAvrg_u32
	- If time distance (dtUseAvrg_u32 - 1us/inc) to last update is longer than this time, input values is divided.

speed could be changed, i.e. with a hex editor like okteta. Takeover of the settings in the files is done through speed change request, i.e. with:
		xinput set-float-prop "TPPS/2 IBM TrackPoint" "libinput Accel Speed" 1

Through this change
- /tmp/setting_libinputTp.dpToFac_au16

values can be set to 1.

With
		sudo ./builddir/libinput record
the /dev/input/event device could be found.

## TPPS/2 IBM TrackPoint

With the default settings of
		/sys/devices/platform/i8042/serio1/serio2/
the values for speed and sensitivity are quite low so at least at my case very low values are send from the kernel/libevdev.

### Setting through udev

Current values from libinput provided rules:
- udevadm info /sys/class/input/event6

POINTINGSTICK_SENSITIVITY is the value which is written to
		/sys/devices/platform/i8042/serio1/serio2/sensitivity

### Changing POINTINGSTICK_SENSITIVITY

At least at debian create
- /etc/udev/hwdb.d/71-pointingstick-local.hwdb

Copy content from
- /usr/lib/udev/hwdb.d/70-pointingstick.hwdb

but set POINTINGSTICK_SENSITIVITY=255

To take over the new settings:
- sudo systemd-hwdb update
- sudo udevadm trigger /sys/class/input/event6

speed has to be set seperatly.

### speed

TODO

## ALPS

Following should be changed:

https://github.com/torvalds/linux/blob/master/drivers/input/mouse/alps.c

alps_process_trackstick_packet_v3()
	/*
	 * The x and y values tend to be quite large, and when used
	 * alone the trackstick is difficult to use. Scale them down
	 * to compensate.
	*/
	x /= 8;
	y /= 8;

alps_process_packet_v6()
	/* Divide 4 since trackpoint's speed is too fast */
	input_report_rel(dev2, REL_X, (char)x / 4);
	input_report_rel(dev2, REL_Y, -((char)y / 4));




libinput
--------

libinput is a library that provides a full input stack for display servers
and other applications that need to handle input devices provided by the
kernel.

libinput provides device detection, event handling and abstraction to
minimize the amount of custom input code the user of libinput needs to
provide the common set of functionality that users expect. Input event
processing includes scaling touch coordinates, generating
relative pointer events from touchpads, pointer acceleration, etc.

User documentation
------------------

Documentation explaining features available in libinput is available
[here](https://wayland.freedesktop.org/libinput/doc/latest/features.html).

This includes the [FAQ](https://wayland.freedesktop.org/libinput/doc/latest/faqs.html)
and the instructions on
[reporting bugs](https://wayland.freedesktop.org/libinput/doc/latest/reporting-bugs.html).


Source code
-----------

The source code of libinput can be found at:
https://gitlab.freedesktop.org/libinput/libinput

For a list of current and past releases visit:
https://www.freedesktop.org/wiki/Software/libinput/

Build instructions:
https://wayland.freedesktop.org/libinput/doc/latest/building.html

Reporting Bugs
--------------

Bugs can be filed on freedesktop.org GitLab:
https://gitlab.freedesktop.org/libinput/libinput/issues/

Where possible, please provide the `libinput record` output
of the input device and/or the event sequence in question.

See https://wayland.freedesktop.org/libinput/doc/latest/reporting-bugs.html
for more info.

Documentation
-------------

- Developer API documentation: https://wayland.freedesktop.org/libinput/doc/latest/development.html
- High-level documentation about libinput's features:
  https://wayland.freedesktop.org/libinput/doc/latest/features.html
- Build instructions:
  https://wayland.freedesktop.org/libinput/doc/latest/building.html
- Documentation for previous versions of libinput: https://wayland.freedesktop.org/libinput/doc/

Examples of how to use libinput are the debugging tools in the libinput
repository. Developers are encouraged to look at those tools for a
real-world (yet simple) example on how to use libinput.

- A commandline debugging tool: https://gitlab.freedesktop.org/libinput/libinput/tree/master/tools/libinput-debug-events.c
- A GTK application that draws cursor/touch/tablet positions: https://gitlab.freedesktop.org/libinput/libinput/tree/master/tools/libinput-debug-gui.c

License
-------

libinput is licensed under the MIT license.

> Permission is hereby granted, free of charge, to any person obtaining a
> copy of this software and associated documentation files (the "Software"),
> to deal in the Software without restriction, including without limitation
> the rights to use, copy, modify, merge, publish, distribute, sublicense,
> and/or sell copies of the Software, and to permit persons to whom the
> Software is furnished to do so, subject to the following conditions: [...]

See the [COPYING](https://gitlab.freedesktop.org/libinput/libinput/tree/master/COPYING)
file for the full license information.

About
-----

Documentation generated from git commit [__GIT_VERSION__](https://gitlab.freedesktop.org/libinput/libinput/commit/__GIT_VERSION__)
